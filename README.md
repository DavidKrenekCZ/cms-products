# Products CMS module

## Installation


### Composer requirement
``` json
composer require davidkrenekcz/product
```

### Package migrations
``` json
php artisan module:migrate Product
```

## Frontend views
You can either use your own views for frontend pages or you can use the default ones located in `Product/Resources/views/frontend`. If you wish to use your own, they must be placed in root views folder in folder `products`.
There are currently two frontend views: **overview.blade** (with *$products* variable) and **detail.blade** (with *$product* variable).