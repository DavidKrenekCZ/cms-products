@extends('layouts.master')

@section('title')
    {{ $product->title }} | @parent
@stop
@section('meta')
    <meta name="title" content="{{ $product->meta_title }}" />
    <meta name="description" content="{{ $product->meta_description }}" />
@stop

@section('content')
    <div class="row">
        <h1>{{ $product->title }}</h1>
        <div>
            @foreach (["title", "price1", "price2", "body", "available"] as $x)
            <b>{{ trans("product::forms.$x") }}:</b> {!! $product->{$x} !!}<br>
            @endforeach
            <b>{{ trans("product::forms.status") }}: </b>{{ $product->status ? "Ano" : "Ne" }}<br>
            <b>{{ trans("product::forms.category") }}: </b>{{ $product->categoryName }}<br>
            <b>{{ trans("product::forms.og_image") }}:</b><br>
            <img src="{{ url("/modules/product/uploads/".$product->product->og_image) }}" style="max-height: 300px">
            <br><br>
            <b>{{ trans("product::forms.gallery") }}:</b><br>
            @foreach($product->product->galleryMediaArray as $file)
                @php
                    $media = DB::table("media__files")->select("*")->where("id", $file->id)->first();
                    // Tohle dělám proto, že mi na lokálu z nějakého důvodu nefunguje MediaPath(), pravděpodobně mám špatně nastavený server
                    // Normálně by mělo stačit níže {{ $file->path }}
                @endphp
                <img src="{{ url($media->path) }}" style="width: 200px">
            @endforeach
        </div>
    </div>
    <script type="text/javascript">
    	$(".datatable").DataTable();
    </script>
@stop
