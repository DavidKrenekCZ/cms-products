@extends('layouts.master')

@section('title')
    Přehled produktů | @parent
@stop
@section('meta')
    <meta name="title" content="" />
    <meta name="description" content="" />
@stop

@section('content')
    <div class="row">
        <h1>Přehled produktů</h1>
        <table class="datatable table-stripped table">
        	<thead>
        		<tr>
        			<th>{{ trans("product::forms.title") }}</th>
        			<th>{{ trans("product::forms.price1") }}</th>
        			<th>{{ trans("product::forms.price2") }}</th>
        			<th>{{ trans("product::forms.og_image") }}</th>
        		</tr>
        	</thead>
        	<tbody>
			@foreach($products as $product)
				<tr>
        			<td>
        				<a href="{{ $product->frontendUrl }}">{{ $product->title }}</a>
        			</td>
        			<td>{{ $product->price1 }}</td>
        			<td>{{ $product->price2 }}</td>
        			<td><img style="max-height: 25px; border: 1px solid black; border-radius: 5px" src="{{ url("modules/product/uploads/".$product->og_image) }}"></td>
        		</tr>
       	 	@endforeach   
        	</tbody>     	
        </table>
    </div>
    <script type="text/javascript">
    	$(".datatable").DataTable();
    </script>
@stop
