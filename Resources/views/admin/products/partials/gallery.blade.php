<div class="modal fade" tabindex="-1" role="dialog" id="product-gallery-modal">
	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title">{{ trans("product::forms.gallery") }}</h4>
      		</div>
      		<div class="modal-body container">
        		<div class="row">
        			@foreach(DB::table("media__files")->select("*")->where([
        					["is_folder", 0],
        					["mimetype", "LIKE", "%image%"]
        				])->get() as $image)
        				<div class="col-md-3 col-sm-4 col-xs-12 image-col">
        					<label for="gallery-checkbox-{{ $image->id }}">
        						<img src="{{ url("$image->path") }}">
        						<span>
        							<input type="checkbox" name="gallery[]" id="gallery-checkbox-{{ $image->id }}" 
        							value="{{ $image->id }}" class="flat-blue"
        							{{ isset($product) && in_array($image->id, $product->galleryIdArray) ? "checked=checked" : "" }}
        							>
        							{{ $image->filename }}
        						</span>
        					</label>
        				</div>
        			@endforeach	
        		</div>
      		</div>
    	</div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(function() {
		$("<span class='gallery-number'>({{ trans("product::forms.chosen") }}: <span></span>)</span>").insertAfter("[data-target='#product-gallery-modal']")

		updateImagesNumber();
		$("#product-gallery-modal input[type=checkbox]").on('ifToggled', updateImagesNumber);
	});

	function updateImagesNumber() {
		$(".gallery-number span").text($("#product-gallery-modal input[type=checkbox]:checked").length);
	}
</script>

@push("css-stack")
	<style>
		#product-gallery-modal .modal-dialog {
			width: 75%;
		}

		@media (max-width: 768px) {
			#product-gallery-modal .modal-dialog {
				width: 90%;
			}
		}

		#product-gallery-modal .image-col {
			text-align: center;
			height: 175px
		}

		#product-gallery-modal .image-col img {
			max-width: 90%;
			max-height: 130px;
		}

		#product-gallery-modal .image-col span {
			display: block;
			padding: 7px 0;
		}

		#product-gallery-modal .image-col span>div {
			margin-right: 5px;
		}
		
		.gallery-number {
			padding-left: 1rem;
		}
	</style>
@endpush