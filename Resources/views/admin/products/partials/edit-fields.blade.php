<div class="box-body">
    <div class="box-body">
        <div class='form-group{{ $errors->has("{$lang}.title") ? ' has-error' : '' }}'>
            {!! Form::i18nInput("title", trans('product::forms.title'), $errors, $lang, $product, ["data-slug" => "source"]) !!}
        </div>
        <div class='form-group{{ $errors->has("{$lang}.slug") ? ' has-error' : '' }}'>
            {!! Form::i18nInput("slug", trans('product::forms.slug'), $errors, $lang, $product, ["data-slug" => "target"]) !!}
        </div>

        {!! $errors->first("{$lang}[body]", '<span class="help-block">:message</span>') !!}

        <div class='form-group{{ $errors->has("{$lang}.status") ? ' has-error' : '' }}'>
            {!! Form::label("{$lang}[status]", trans('product::forms.status')) !!}
            {!! Form::i18nCheckbox("status", trans('product::forms.status'), $errors, $lang, $product) !!}
        </div>

        <div class='form-group{{ $errors->has("{$lang}.available") ? ' has-error' : '' }}'>
            {!! Form::i18nInput("available", trans('product::forms.available'), $errors, $lang, $product) !!}
        </div>

        <div class='form-group{{ $errors->has("{$lang}.price1") ? ' has-error' : '' }}'>
            {!! Form::i18nInput("price1", trans('product::forms.price1'), $errors, $lang, $product) !!}
        </div>

        <div class='form-group{{ $errors->has("{$lang}.price2") ? ' has-error' : '' }}'>
            {!! Form::i18nInput("price2", trans('product::forms.price2'), $errors, $lang, $product) !!}
        </div>

        <div class="form-group{{ $errors->has("category]") ? ' has-error' : '' }}">
            <label>{{ trans('product::forms.category') }}</label>
            <select class="form-control" name="{{ $lang }}[category]">
                <option value="1" {{ old("$lang.category") == '1' ? 'selected' : '' }}>{{ trans('product::forms.categories.1') }}</option>
                <option value="2" {{ old("$lang.category") == '2' ? 'selected' : '' }}>{{ trans('product::forms.categories.2') }}</option>
                <option value="3" {{ old("$lang.category") == '3' ? 'selected' : '' }}>{{ trans('product::forms.categories.3') }}</option>
            </select>
        </div>

        @editor('body', trans('product::forms.body'), old("{$lang}.body", $product->hasTranslation($lang) ? $product->translate($lang)->body : ''), $lang)

 {{--   @php if (config('asgard.product.config.partials.translatable.create') !== []): @endphp
            @php foreach (config('asgard.product.config.partials.translatable.create') as $partial): @endphp
                @include($partial)
            @php endforeach; @endphp
        @php endif; @endphp--}}
    </div>
    <div class="box-group" id="accordion">
        <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
        <div class="panel box box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo-{{$lang}}">
                        {{ trans('product::forms.meta_data') }}
                    </a>
                </h4>
            </div>
            <div style="height: 0px;" id="collapseTwo-{{$lang}}" class="panel-collapse collapse">
                <div class="box-body">
                    <div class='form-group{{ $errors->has("meta_title]") ? ' has-error' : '' }}'>
                        {!! Form::i18nInput("meta_title", trans('product::forms.meta_title'), $errors, $lang, $product) !!}
                    </div>
                    <div class='form-group{{ $errors->has("meta_description]") ? ' has-error' : '' }}'>
                        {!! Form::label("{$lang}meta_description", trans('product::forms.meta_description')) !!}
                        <textarea class="form-control" name="{{$lang}}[meta_description]" rows="10" cols="80">{{ old("$lang.meta_description") }}</textarea>
                        {!! $errors->first("{$lang}[meta_description]", '<span class="help-block">:message</span>') !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="panel box box-primary">
            <div class="box-header">
                <h4 class="box-title">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFacebook-{{$lang}}">
                        {{ trans('product::forms.facebook_data') }}
                    </a>
                </h4>
            </div>
            <div style="height: 0px;" id="collapseFacebook-{{$lang}}" class="panel-collapse collapse">
                <div class="box-body">
                    <div class='form-group{{ $errors->has("og_title]") ? ' has-error' : '' }}'>
                        {!! Form::i18nInput("og_title", trans('product::forms.og_title'), $errors, $lang, $product) !!}
                    </div>
                    <div class='form-group{{ $errors->has("og_description]") ? ' has-error' : '' }}'>
                        {!! Form::label("{$lang}[og_description]", trans('product::forms.og_description')) !!}
                        <textarea class="form-control" name="{{$lang}}[og_description]" rows="10" cols="80">{{ old("$lang.og_description") }}</textarea>
                        {!! $errors->first("og_description]", '<span class="help-block">:message</span>') !!}
                    </div>
                    <div class="form-group{{ $errors->has("og_type]") ? ' has-error' : '' }}">
                        <label>{{ trans('product::forms.og_type') }}</label>
                        <select class="form-control" name="{{ $lang }}[og_type]">
                            <option value="website" {{ old("$lang.og_type") == 'website' ? 'selected' : '' }}>{{ trans('product::forms.facebook_types.website') }}</option>
                            <option value="product" {{ old("$lang.og_type") == 'product' ? 'selected' : '' }}>{{ trans('product::forms.facebook_types.product') }}</option>
                            <option value="article" {{ old("$lang.og_type") == 'article' ? 'selected' : '' }}>{{ trans('product::forms.facebook_types.article') }}</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>