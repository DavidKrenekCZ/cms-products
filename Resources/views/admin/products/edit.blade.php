@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('product::products.title.edit product') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.product.product.index') }}">{{ trans('product::products.title.products') }}</a></li>
        <li class="active">{{ trans('product::products.title.edit product') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.product.product.update', $product->id], 'method' => 'put', 'files' => true]) !!}
    <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('product::admin.products.partials.edit-fields', ['lang' => $locale])
                        </div>
                    @endforeach
                    <hr>

                    <div class="box-footer">
                        <button type="submit" class="el-button el-button--primary">{{ trans('core::core.button.update') }}</button>
                        <a class="el-button el-button--danger" href="{{ route('admin.product.product.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class='image-form-group form-group{{ $errors->has("change_image") ? ' has-error' : '' }}'>
                        {!! Form::label("og_image", trans('product::forms.og_image')) !!}
                        <div class="checkbox">
                            <label for="change_image">
                            {!! Form::checkbox("change_image", 1, 0, ["class" => "flat-blue"]) !!}
                            {{ trans('product::forms.change_photo') }}
                            </label>
                        </div>
                        {!! Form::file("og_image") !!}
                        <a href="{{ url("/modules/product/uploads/".$product->og_image) }}" target="_blank">
                            <img src="{{ url("/modules/product/uploads/".$product->og_image) }}">
                        </a>
                        {!! $errors->first("change_image", '<span class="help-block">:message</span>') !!}
                    </div>
                    <hr style="margin: 10px 0">
                    <button type="button" class="el-button el-button--primary" data-toggle="modal" data-target="#product-gallery-modal"><i class="fa fa-picture-o"></i> {{ trans("product::forms.gallery") }}</button>
                </div>

                @include("product::admin.products.partials.gallery")
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "@php route('admin.product.product.index') @endphp" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
    <style>
        .form-group.image-form-group >label {
            display: block;
        }

        .form-group.image-form-group >.checkbox {
            display: inline-block;
        }

        .form-group.image-form-group >input[type="file"] {
            display: inline-block;
            padding: 0 0 10px;
        }

        .form-group.image-form-group.has-error .checkbox label {
            color: black !important;
        }

        .form-group.image-form-group.has-error input[type=file] {
            color: #dd4b39 !important;
        }

        .form-group.image-form-group>a>img {
            height: 40px;
            max-width: 100px;
            border-radius: 5px;
            border: 1px solid black;
            display: block;
            margin: 0 auto;
        }
    </style>
@endpush
