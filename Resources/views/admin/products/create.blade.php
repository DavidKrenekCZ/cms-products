@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('product::products.title.create product') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.product.product.index') }}">{{ trans('product::products.title.products') }}</a></li>
        <li class="active">{{ trans('product::products.title.create product') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.product.product.store'], 'method' => 'post', 'files' => true]) !!}
    <div class="row">
        <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('product::admin.products.partials.create-fields', ['lang' => $locale])
                        </div>
                    @endforeach
                    <hr>

                    <div class="box-footer">
                        <button type="submit" class="el-button el-button--primary">{{ trans('core::core.button.create') }}</button>
                        <a class="el-button el-button--danger" href="{{ route('admin.product.product.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="box box-primary">
                <div class="box-body">
                    <div class='form-group{{ $errors->has("change_image") ? ' has-error' : '' }}'>
                        {!! Form::label("og_image", trans('product::forms.og_image')) !!}
                        {!! Form::hidden("change_image", 1) !!} 
                        {!! Form::file("og_image") !!}
                        {!! $errors->first("change_image", '<span class="help-block">:message</span>') !!}
                    </div>
                    <hr style="margin: 10px 0">
                    <button type="button" class="el-button el-button--primary" data-toggle="modal" data-target="#product-gallery-modal"><i class="fa fa-picture-o"></i> {{ trans("product::forms.gallery") }}</button>
                </div>

                @include("product::admin.products.partials.gallery")
            </div>
        </div>        
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "@php route('admin.product.product.index') @endphp" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush
