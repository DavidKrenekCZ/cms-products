<?php

return [
    'list resource' => 'Vypsat produkty',
    'create resource' => 'Vytvořit produkty',
    'edit resource' => 'Upravit produkty',
    'destroy resource' => 'Smazat produkty',
    'title' => [
        'products' => 'Produkt',
        'create product' => 'Vytvořit produkt',
        'edit product' => 'Upravit produkt',
    ],
    'button' => [
        'create product' => 'Vytvořit produkt',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
