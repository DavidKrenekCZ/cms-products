<?php

return [
    // Form
    'title' => 'Titulek',
    'slug' => 'Zkratka URL',
    'body' => 'Obsah',
    'meta_data' => 'Meta data',
    'facebook_data' => 'Facebook data',
    'meta_title' => 'Meta titulek',
    'meta_description' => 'Meta popis',
    'og_title' => 'Facebook titulek',
    'og_description' => 'Facebook popis',
    'og_type' => 'Facebook typ',
    'facebook_types' => [
    	'website' => 'Stránka',
    	'product' => 'Produkt',
    	'article' => 'Článek' 
    ],
    'og_image' => 'Hlavní fotka',
    'available' => 'K dispozici',
    'price1' => 'Cena v původním stavu',
    'price2' => 'Orientační cena po repasi',
    'category' => 'Kategorie',
    'status' => 'Prodáno',
    'categories' => [
    	'1' => 'Kategorie 1',
    	'2' => 'Kategorie 2',
    	'3' => 'Kategorie 3'
    ],
    'change_photo' => 'Změnit fotku',
    "chosen"        => "vybráno",
    "gallery"       => "Galerie"
];
