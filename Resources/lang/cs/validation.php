<?php

return [
    'required' 		=> 'Pole musí být vyplněné!',
    'integer' 		=> 'Pole musí být číslo!',
    'min'			=> 'Pole musí být dlouhé alespoň :min znaky.',
    'main_image'	=> 'Nahrajte soubor ve formátu .jpg, .png nebo .gif'
];
