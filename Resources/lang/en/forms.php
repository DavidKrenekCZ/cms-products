<?php

return [
    // Form
    'title' => 'Title',
    'slug' => 'Slug',
    'body' => 'Body',
    'meta_data' => 'Meta data',
    'facebook_data' => 'Facebook data',
    'meta_title' => 'Meta title',
    'meta_description' => 'Meta description',
    'og_title' => 'Facebook title',
    'og_description' => 'Facebook description',
    'og_type' => 'Facebook type',
    'facebook_types' => [
    	'website' => 'Website',
    	'product' => 'Product',
    	'article' => 'Article' 
    ],
    'og_image' => 'Image',
    'available' => 'Available',
    'price1' => 'Original price',
    'price2' => 'Informative price after repossession',
    'category' => 'Category',
    'status' => 'Sold',
    'categories' => [
    	'1' => 'Category 1',
    	'2' => 'Category 2',
    	'3' => 'Category 3'
    ],
    'change_photo'  => "Change photo",
    "chosen"        => "chosen",
    "gallery"       => "Gallery"
];
