<?php

return [
    'required' 			=> 'The field is required!',
    'integer'	 		=> 'The field must be an integer!',
    'min'				=> 'The field must be at least :min characters long!',
    'main_image'		=> 'Please upload .jpg, .png or .gif file!'
];
