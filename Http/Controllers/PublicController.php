<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Menu\Repositories\MenuItemRepository;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductTranslation;
use Modules\Product\Repositories\ProductRepository;

class PublicController extends BasePublicController
{
    /**
     * @var ProductRepository
     */
    private $product;
    /**
     * @var Application
     */
    private $app;

    public function __construct(ProductRepository $product, Application $app)
    {
        parent::__construct();
        $this->product = $product;
        $this->app = $app;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function overview()
    {
        $products = $this->product->all();

        $this->throw404IfNotFound($products);

        if (view()->exists("products.overview"))
            return view('products.overview', compact('products'));
        return view('product::frontend.overview', compact('products'));
    }

    public function detail($id, $slug) {
        $product = ProductTranslation::where([
            "product_id" => $id,
            "slug" => $slug
        ])->firstOrFail();

        if (view()->exists("products.detail"))
            return view('products.detail', compact('product'));
        return view('product::frontend.detail', compact('product'));
    }

    /**
     * Find a product for the given slug.
     * The slug can be a 'composed' slug via the Menu
     * @param string $slug
     * @return Product
     */
    private function findProductForSlug($slug)
    {
        $menuItem = app(MenuItemRepository::class)->findByUriInLanguage($slug, locale());

        if ($menuItem) {
            return $this->product->find($menuItem->product_id);
        }

        return $this->product->findBySlug($slug);
    }

    /**
     * Throw a 404 error product if the given product is not found
     * @param $product
     */
    private function throw404IfNotFound($product)
    {
        if (is_null($product)) {
            $this->app->abort('404');
        }
    }
}
