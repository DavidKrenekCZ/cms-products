<?php

namespace Modules\Product\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Product\Entities\Product;
use Modules\Product\Http\Requests\CreateProductRequest;
use Modules\Product\Http\Requests\UpdateProductRequest;
use Modules\Product\Repositories\ProductRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class ProductController extends AdminBaseController
{
    /**
     * @var ProductRepository
     */
    private $product;

    public function __construct(ProductRepository $product)
    {
        parent::__construct();

        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $products = $this->product->all();

        return view('product::admin.products.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('product::admin.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateProductRequest $request
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        $data = $this->saveImage($request->all());
        $data = $this->parseGallery($data);
        $this->product->create($data);

        return redirect()->route('admin.product.product.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('product::products.title.products')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Product $product
     * @return Response
     */
    public function edit(Product $product)
    {
        return view('product::admin.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Product $product
     * @param  UpdateProductRequest $request
     * @return Response
     */
    public function update(Product $product, UpdateProductRequest $request)
    {
        $data = $this->saveImage($request->all());
        $data = $this->parseGallery($data);
        $this->product->update($product, $data);

        return redirect()->route('admin.product.product.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('product::products.title.products')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product $product
     * @return Response
     */
    public function destroy(Product $product)
    {
        $this->product->destroy($product);

        return redirect()->route('admin.product.product.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('product::products.title.products')]));
    }

    private function parseGallery($data) {
        
        if (isset($data["gallery"]))
            $data["gallery"] = implode(";", $data["gallery"]);
        else
            $data["gallery"] = "";

        return $data;
    }

    private function saveImage($data) {
        $imgDir = public_path("modules/product/uploads");

        if (!file_exists(public_path("modules/product")))
            mkdir(public_path("modules/product"));
        if (!file_exists($imgDir))
            mkdir($imgDir);

        // DEBUG: if fields are not set, replace with czech ones or with default values
        foreach ($data as $index => $lang) {
            if ($index != "cs" && is_array($lang) && $index != "gallery") {
                $required = ["title", "slug", "body", "price1", "price2"];
                foreach ($required as $value) {
                    if (!isset($data["cs"][$value]) || trim($data["cs"][$value]) == "" || strlen($data["cs"][$value]) < 3) {
                        if ($value == "price1" || $value == "price2")
                            $data["cs"][$value] = "0";
                        else
                            $data["cs"][$value] = "   ";
                    }

                    if (!isset($lang[$value]) || trim($lang[$value]) == "" || strlen($lang[$value]) < 3)
                        $data[$index][$value] = $data["cs"][$value];
                }
            }
        }
         
        if (isset($data["change_image"]) && isset($data["og_image"])) {
            $file = $data["og_image"];
            if ($file->extension() == "jpeg" || $file->extension() == "png" || $file->extension() == "gif") {
                $ext = $file->clientExtension();
                if ($ext == "jpeg")
                    $ext = "jpg";
                $name = 
                    date("Y-m-d")."-".
                    time()."-".
                    str_random(32)."-".
                    mt_rand(100000, 999999).
                    ".".$ext;
                $file->move($imgDir, $name);
                $data["og_image"] = $name;
            }
        }

        return $data;
    }
}
