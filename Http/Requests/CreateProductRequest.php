<?php

namespace Modules\Product\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateProductRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'change_image' => 'main_image',
        ];
    }

    public function translationRules()
    {
        return [];
        /*
        return [
            'title' => 'required|min:3',
            'slug' => 'required|min:3',
            'body' => 'required|min:3',
            'price1' => 'required|integer|min:3',
            'price2' => 'required|integer|min:3',
        ];
        */
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'required' => trans('product::validation.required'),
            'integer' => trans('product::validation.integer'),
            'min' => trans('product::validation.min'),
            'main_image' => trans('product::validation.main_image'),
        ];
    }

    public function translationMessages()
    {
        return [];
    }
}
