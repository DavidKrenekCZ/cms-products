<?php

use Illuminate\Routing\Router;

/** @var Router $router */
if (! App::runningInConsole()) {
    $router->get('/produkty', [
        'uses' => 'PublicController@overview',
        'as' => 'products.overview',
    ]);
    $router->get('/produkt/{id}-{slug}', [
        'uses' => 'PublicController@detail',
        'as' => 'products.detail',
    ]);
}
