<?php

namespace Modules\Product\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Input;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Validator::extend('main_image', function ($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();

            if (isset($data['change_image']) && $data['change_image']) {
                $request = app(\Illuminate\Http\Request::class);
                if (!isset($request["og_image"]))
                    return false;

                switch ($request["og_image"]->extension()) {
                    case 'png':
                    case 'jpeg':
                    case 'gif':
                        return true;
                        break;
                    
                    default:
                        return false;
                }
            }

            return true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
