<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [
		'product_id',
        'title',
        'slug',
        'status',
        'body',
        'meta_title',
        'meta_description',
        'og_title',
        'og_description',
        'og_image',
        'og_type',
        'available',
        'price1',
        'price2',
        'category'
    ];
    protected $table = 'product__product_translations';
    protected $appends = ["categoryName"];

    public function product() {
        return $this->belongsTo("\Modules\Product\Entities\Product");
    }

    public function getCategoryNameAttribute() {
        return trans("product::forms.categories.".$this->category);
    }
}
