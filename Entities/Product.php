<?php

namespace Modules\Product\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use Translatable;

    protected $table = 'product__products';
    public $translatedAttributes = [
    	'product_id',
        'title',
        'slug',
        'status',
        'body',
        'meta_title',
        'meta_description',
        'og_title',
        'og_description',
        'og_type',
        'available',
        'price1',
        'price2',
        'category'
    ];
    protected $fillable = [
        'og_image',
        'gallery',
    	// Translated
    	'product_id',
        'title',
        'slug',
        'status',
        'body',
        'meta_title',
        'meta_description',
        'og_title',
        'og_description',
        'og_type',
        'available',
        'price1',
        'price2',
        'category'
    ];

    protected $appends = ["frontendUrl", "galleryIdAray", "galleryMediaArray"];

    public function getFrontendUrlAttribute() {
        return url(locale()."/produkt/".$this->id."-".$this->slug);
    }

    // Array of IDs of files in product gallery
    public function getGalleryIdArrayAttribute() {
        return explode(";", $this->gallery);
    }

    // Array of File instances of files in product gallery
    public function getGalleryMediaArrayAttribute() {
        $array = [];

        foreach (explode(";", $this->gallery) as $id) {
            $media = \Modules\Media\Entities\File::find($id);
            if (count($media))
                $array[] = $media;
        }

        return $array;
    }
}
