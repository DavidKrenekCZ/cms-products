<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ChangeAvailableType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product__product_translations', function (Blueprint $table) {
            $table->dropColumn('available');
        });

        Schema::table('product__product_translations', function (Blueprint $table) {
            $table->string('available')->default("")->after("og_type");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product__product_translations', function (Blueprint $table) {
            $table->dropColumn('available');
        });

        Schema::table('product__product_translations', function (Blueprint $table) {
            $table->boolean("available")->default(0)->after("og_type");
        });
    }
}
