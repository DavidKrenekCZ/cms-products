<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product__product_translations', function (Blueprint $table) {
            $table->dropColumn('og_image');
        });

        Schema::table('product__products', function (Blueprint $table) {
            $table->string('og_image')->nullable()->after("id");
            $table->string('gallery', 10000)->nullable()->after("id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
